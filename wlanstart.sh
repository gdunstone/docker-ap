#!/bin/bash -e

# Check if running in privileged mode
if [ ! -w "/sys" ] ; then
echo "[Error] Not running in privileged mode."
exit 1
fi

# Default values
true ${INTERFACE:=wlan0}
true ${OUTGOING:=eth0}
true ${SUBNET:=192.168.254.0}
true ${AP_ADDR:=192.168.254.1}
true ${SSID:=docker-ap}
true ${CHANNEL:=11}
true ${AC_CHANNEL:=36}
true ${WPA_PASSPHRASE:=passw0rd}
true ${HW_MODE:=g}
true ${DRIVER:=nl80211}
true ${HT_CAPAB:=[HT40-][SHORT-GI-20]}
true ${AP_BAND:=802.11n}
true ${MODE:=host}

# Attach interface to container in guest mode
if [ "$MODE" == "guest"  ]; then

    echo "Attaching interface to container"

    CONTAINER_ID=$(cat /proc/self/cgroup | grep -o  -e "/docker/.*" | head -n 1| sed "s/\/docker\/\(.*\)/\\1/")
    CONTAINER_PID=$(docker inspect -f '{{.State.Pid}}' ${CONTAINER_ID})
    CONTAINER_IMAGE=$(docker inspect -f '{{.Config.Image}}' ${CONTAINER_ID})

    docker run -t \
        --privileged \
        --net=host \
        --pid=host \
        --rm \
        --entrypoint /bin/ash ${CONTAINER_IMAGE} \
        -c "PHY=\$(echo phy\$(iw dev ${INTERFACE} info | grep wiphy | tr ' ' '\n' | tail -n 1));\iw phy \$PHY set netns ${CONTAINER_PID};"

    ip link set ${INTERFACE} name wlan0
    
    INTERFACE=wlan0



fi

mkdir -p /etc/hostapd/


if [ ! -f "/etc/hostapd/5ghz.conf" ] ; then

cat > "/etc/hostapd/5ghz.conf" <<EOF
interface=${INTERFACE}
driver=${DRIVER}
ssid=${SSID}
utf8_ssid=1
wpa=2
wpa_passphrase=${WPA_PASSPHRASE}
wpa_key_mgmt=WPA-PSK
# TKIP is no secure anymore
#wpa_pairwise=TKIP CCMP
wpa_pairwise=CCMP
rsn_pairwise=CCMP
wpa_ptk_rekey=600

country_code=AU
hw_mode=a

# N
ieee80211n=1
require_ht=1
ht_capab=[MAX-AMSDU-3839][HT40+][SHORT-GI-20][SHORT-GI-40][DSSS_CCK-40]

# AC
ieee80211ac=1
require_vht=1
ieee80211d=0
ieee80211h=0
vht_capab=[MAX-AMSDU-3839][SHORT-GI-80]
vht_oper_chwidth=1

# the raspberry pis wireless card does not support ACS, 
# do not set this to 0 or acs_survey
channel=${AC_CHANNEL}
vht_oper_centr_freq_seg0_idx=42

ignore_broadcast_ssid=0

EOF

fi


if [ ! -f "/etc/hostapd/2ghz.conf" ] ; then

cat > "/etc/hostapd/2ghz.conf" <<EOF
interface=${INTERFACE}
driver=${DRIVER}
ssid=${SSID}
utf8_ssid=1
wpa=2
wpa_passphrase=${WPA_PASSPHRASE}
wpa_key_mgmt=WPA-PSK
# TKIP is no secure anymore
#wpa_pairwise=TKIP CCMP
wpa_pairwise=CCMP
rsn_pairwise=CCMP
wpa_ptk_rekey=600

country_code=AU
hw_mode=g
# the raspberry pis wireless card does not support ACS, 
# do not set this to 0 or acs_survey
channel=${CHANNEL}
wmm_enabled=1

# N
ieee80211d=1
ieee80211n=1
require_ht=1
ht_capab=[HT40+][SHORT-GI-20][SHORT-GI-40][DSSS_CCK-40][MAX-AMSDU-3839]

ignore_broadcast_ssid=0

EOF

fi
# unblock wlan
rfkill unblock wlan

echo "Setting interface ${INTERFACE}"

# Setup interface and restart DHCP service 
ip link set ${INTERFACE} up
ip addr flush dev ${INTERFACE}
ip addr add ${AP_ADDR}/24 dev ${INTERFACE}

# NAT settings
echo "NAT settings ip_dynaddr, ip_forward"

for i in ip_dynaddr ip_forward ; do
if [ $(cat /proc/sys/net/ipv4/$i) ]; then
echo $i already 1
else
echo "1" > /proc/sys/net/ipv4/$i
fi
done

cat /proc/sys/net/ipv4/ip_dynaddr
cat /proc/sys/net/ipv4/ip_forward

echo "Setting iptables for outgoing traffic on ${OUTGOING}..."
iptables -t nat -A POSTROUTING -o ${OUTGOING} -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i ${INTERFACE} -o ${OUTGOING} -j ACCEPT

echo "Setting iptables for client-client communication..."
iptables -A FORWARD -s ${SUBNET}/24 -j ACCEPT

echo "Configuring DHCP server .."
#option domain-name-servers 8.8.8.8, 8.8.4.4;

cat > "/etc/dhcp/dhcpd.conf" <<EOF
option domain-name-servers ${AP_ADDR};
option subnet-mask 255.255.255.0;
option routers ${AP_ADDR};
subnet ${SUBNET} netmask 255.255.255.0 {
range ${SUBNET::-1}100 ${SUBNET::-1}200;
}
EOF

echo "Starting CoreDNS"
coredns -conf /etc/coredns/Corefile &

echo "Starting DHCP server .."
dhcpd ${INTERFACE}

echo "Starting HostAP daemon ..."
if [ "$AP_BAND" == "802.11ac"  ]; then
    /usr/sbin/hostapd /etc/hostapd/5ghz.conf 
elif [ "$AP_BAND" == "both" ]; then
    /usr/sbin/hostapd /etc/hostapd/5ghz.conf /etc/hostapd/2ghz.conf 
else
    /usr/sbin/hostapd /etc/hostapd/2ghz.conf 
fi