# FROM golang:1.14 as coredns_build
# WORKDIR /build
# RUN git clone --depth 1 https://github.com/coredns/coredns && \
#     cd coredns && \
#     make

FROM alpine:3.11
RUN apk add --no-cache bash hostapd iptables dhcp docker iproute2 iw wget
RUN mkdir -p /etc/coredns
RUN ARCH=`uname -m` && \
    if [ "$ARCH" == "x86_64" ]; then \
       echo "x86_64" && \
       wget --quiet https://github.com/coredns/coredns/releases/download/v1.8.3/coredns_1.8.3_linux_amd64.tgz -O /coredns.tgz; \
    elif [ "$ARCH" == "armv7l" ]; then \
       echo "arm" && \
       wget --quiet https://github.com/coredns/coredns/releases/download/v1.8.3/coredns_1.8.3_linux_arm.tgz -O /coredns.tgz; \
    elif [ "$ARCH" == "armv6l" ]; then \
       echo "arm" && \
       wget --quiet https://github.com/coredns/coredns/releases/download/v1.8.3/coredns_1.8.3_linux_arm.tgz -O /coredns.tgz; \
    fi
RUN tar xvzf /coredns.tgz && cp coredns /bin/ && chmod +x /bin/coredns
COPY Corefile /etc/coredns/Corefile
# COPY --from=coredns_build /build/coredns/coredns /bin/

RUN echo "" > /var/lib/dhcp/dhcpd.leases
COPY wlanstart.sh /bin/wlanstart.sh
ENTRYPOINT [ "/bin/wlanstart.sh" ]
